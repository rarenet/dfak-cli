#!/usr/bin/env node
'use strict'
const resolveGlobal = require('resolve-global')
const exec = require('child_process').spawn

// var child = null

try {
  const docsmithPath = resolveGlobal('docsmith')

  // child = exec(
  //   `content --argv1 ${process.argv[1]} ${process.argv.slice(2).join(' ')}`,
  //   {
  //     stdio: 'pipe',
  //   }
  // )

  var child = exec(
    'content',
    ['--argv1', process.argv[1], ...process.argv.slice(2)],
    {
      stdio: 'inherit',
    }
  )
  process.on('SIGINT', function() {
    if (child !== null) child.kill('SIGINT')
    process.exit()
  })

  // child.stdout.on('data', data => {
  //   console.log(`stdout: ${data}`)
  // })

  // child.stderr.on('data', data => {
  //   console.error(`stderr: ${data}`)
  // })

  // child.on('close', code => {
  //   console.log(`child process exited with code ${code}`)
  // })
} catch (e) {
  if (e.code === 'MODULE_NOT_FOUND') {
    console.error('Missing required dependency.')
    console.error(
      'Please install docsmith globally with `npm i -g docsmith@beta` before running this command.'
    )
  }
  // assume error was handled by child process
  throw e
}
